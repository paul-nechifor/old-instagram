const fs = require('fs');
const mkdirp = require('mkdirp');
const sharp = require('sharp');
const util = require('util');
const childProcess = require('child_process');

const exec = util.promisify(childProcess.exec);

const imageRoot = "images";
const distImages = "dist/images";

async function main() {
  await mkdirp(distImages);
  const images = fs.readdirSync(imageRoot);

  images.forEach(async image => {
    const fullPath = `${imageRoot}/${image}`;
    const name = image.split('.')[0];
    const imagePath = `${distImages}/${name}.jpg`;
    const thumbPath = `${distImages}/${name}.thumb.jpg`;
    await writeImage(fullPath, imagePath, 1600, 80);
    await writeImage(fullPath, thumbPath, 265, 90);
  });
}

async function writeImage(src, dst, size, quality) {
  await sharp(src)
    .resize(size)
    .jpeg({quality, optimizeScans: true})
    .toFile(dst);
  let ret = await exec(`exiftool -all= '${dst}'`);
  console.log(ret.stdout, ret.stderr);
  ret = await exec(`exiftool -author="Paul Nechifor <paul@nechifor.net>" -copyright="Paul Nechifor (nechifor.net/old-instagram)" -overwrite_original '${dst}'`);
  console.log(ret.stdout, ret.stderr);
}

main();
